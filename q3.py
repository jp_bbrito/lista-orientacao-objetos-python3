#!/usr/bin/python3
"""
3. Classe Retangulo: Crie uma classe que modele um retangulo:
a. Atributos: LadoA, LadoB (ou Comprimento e Largura, ou Base e Altura, a escolher)
b. Métodos: Mudar valor dos lados, Retornar valor dos lados, calcular Área e calcular Perímetro;
c. Crie um programa que utilize esta classe. Ele deve pedir ao usuário que informe as medidades 
de um local. Depois, deve criar um objeto com as medidas e calcular a quantidade de pisos e de 
rodapés necessárias para o local.
"""

class Retangulo(object):
    def __init__(self,comprimento,largura):
        self.comprimento = comprimento
        self.largura = largura
        return print("Largura %f e comprimento %f"% (self.largura, self.comprimento))

    def mostrarRetangulo(self):
        return print("Largura %f e comprimento %f"% (self.largura, self.comprimento))

    def mudarLados(self):
        aux = comprimento
        self.comprimento = self.largura
        self.largura = aux
        return print("Largura %f e comprimento %f"% (self.largura, self.comprimento))

    def calcularArea(self):
        area = self.comprimento * self.largura
        return print("Area do retangulo %f"% area)

    def perimetro(self):
        perimetro = self.comprimento*2 + self.largura*2
        return print("Perimetro %f"% perimetro)


while True:
    print(" ------------- Menu ------------- ")
    op = input("(1) - Criar retangulo (2) - Ver valores (3) - Calcular Area (4) Mudar lados (5) Perimetro  (6) Sair ")
    if op == '1':
        comprimento = float(input("Digite o valor do comprimento: " ))
        largura = float(input("Digite o valor do comprimento: "))
        a = Retangulo(comprimento,largura)
    elif op == '2':
        a.mostrarRetangulo()
    elif op == '3':
        a.calcularArea()
    elif op == '4':
        a.mudarLados()
    elif op == '5':
        a.perimetro()
    elif op == '6':
        break
    else:
        continue
    
