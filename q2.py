"""
2. Classe Quadrado: Crie uma classe que modele um quadrado:
	a. Atributos: Tamanho do lado
	b. Métodos: Mudar valor do Lado, Retornar valor do Lado e calcular Área.
"""

class Quadrado(object):
	def __init__(self, lado):
		self.lado = lado
		
	def mudarValor(self, lado):
		self.lado = lado
		return (print('Novo valor do lado %d'%self.lado))
	
	def mostraValor(self):
		return (print('Valor do lado: %d'%self.lado))
		
	def calculaArea(self):
		print('Area do quadrado: %d'%(self.lado*self.lado))
		return self.lado*self.lado
		
obj1 = Quadrado(6)
obj1.mostraValor()
obj1.mudarValor(8)
x = obj1.calculaArea()

print(x)

	
