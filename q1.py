"""
1. Classe Bola: Crie uma classe que modele uma bola:
	a. Atributos: Cor, circunferência, material
	b. Métodos: trocaCor e mostraCor
"""

class Bola(object):
	def __init__(self, c = 'Azul', cir = 5, mat = 'Couro'):
		self.cor = c 
		self.circuferencia = cir
		self.material = mat
		print('Objeto instanciado!')
		
	def trocaCor(self, c):
		self.cor = c
		print('Nova cor: %s'%(self.cor))
	
	def mostraCor(self):
		print('Cor da bola: %s'%(self.cor))
		
obj1 = Bola()
obj1.mostraCor()
obj1.trocaCor('Verde Limão')

obj2 = Bola()
obj2.mostraCor()
obj2.trocaCor('Vermelho')
